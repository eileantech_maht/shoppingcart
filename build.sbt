name := "ShoppingCart"

version := "0.1"

scalaVersion := "2.13.6"

lazy val ShoppingCart = project in file(".")

libraryDependencies in ShoppingCart ++= Seq(
  "org.scalatest" %% "scalatest" % "latest.integration" % "test"
)
