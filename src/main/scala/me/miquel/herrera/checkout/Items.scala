package me.miquel.herrera.checkout

object Items {

  /** Represents an item from the shop */
  trait Item {
    def name:String
  }

  object Item {
    def apply(name: String): Item = DefaultItem(name)
  }

  case class DefaultItem(name: String) extends Item

  /**
   * Represents an item with a price attached to it
   */
  case class PricedItem(name: String, price: Long) extends Item

  /**
   * Convenience class to represent a batch of items of the same type
   * @param pricedItem The template item in the batch with its price
   * @param quantity how many of those items we have
   */
  case class PricedItemBatch(pricedItem: PricedItem, quantity: Int)

}