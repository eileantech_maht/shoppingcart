package me.miquel.herrera.checkout.pricing

import me.miquel.herrera.checkout.Items.{Item, PricedItem, PricedItemBatch}

/**
 * Service providing prices for items. For this PoC prices will just be stored in a map
 * Price units in pence
 */
case class PricingService(prices: Map[String, Long]) extends Pricing {

  override def priceItems(items: Seq[Item]): Seq[PricedItem] = {
    val (pricedItems, missingItems) = items.foldLeft((Seq[PricedItem](), Seq[Item]())){ case (res, itm) =>
      prices.get(itm.name).
        map(p => res.copy(_1 = res._1 :+ PricedItem(itm.name, p)))
        .getOrElse(res.copy(_2 = res._2 :+ itm))
    }

    if (missingItems.nonEmpty)
      throw new RuntimeException(s"Some items did not have a price associated: [${missingItems.map(_.name).mkString(", ")}]")

    pricedItems
  }

  override def aggregatedPrice(items: Seq[Item]): Seq[PricedItemBatch] = {
    val grouped = items.groupBy(_.name)
    val priceMap = priceItems(grouped.map{ case (_, itms) => itms.head }.toSeq).groupBy(_.name)

    grouped.map{ case (name, itms) => PricedItemBatch(priceMap(name).head, itms.size) }.toSeq
  }
}
