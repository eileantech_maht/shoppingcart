package me.miquel.herrera.checkout.pricing

import me.miquel.herrera.checkout.Items.{Item, PricedItem, PricedItemBatch}

trait Pricing {

  def priceItems(items: Seq[Item]): Seq[PricedItem]

  def aggregatedPrice(items: Seq[Item]): Seq[PricedItemBatch]
}

object OfferTypes {
  def buyOneGetOneFree(batch: PricedItemBatch): Long = {
    import batch._
    (quantity * pricedItem.price) - ((quantity / 2) * pricedItem.price)
  }

  def threeForTwo(batch: PricedItemBatch): Long = {
    import batch._
    (quantity * pricedItem.price) - ((quantity / 3) * pricedItem.price)
  }
}