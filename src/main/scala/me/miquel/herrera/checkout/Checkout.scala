package me.miquel.herrera.checkout

import me.miquel.herrera.checkout.Items.{Item, PricedItemBatch}

trait Checkout {
  def checkout(items: Seq[Item]): Long

  def checkout(items: Seq[Item], batchOffers: Map[String, PricedItemBatch => Long]): Long

}
