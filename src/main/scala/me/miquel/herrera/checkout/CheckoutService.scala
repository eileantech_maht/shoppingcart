package me.miquel.herrera.checkout

import me.miquel.herrera.checkout.Items.{Item, PricedItemBatch}
import me.miquel.herrera.checkout.pricing.Pricing

/**
 * Checkout Service. Expects injection of a pricingService
 *
 * @param pricingService Service providing prices to items
 */
case class CheckoutService(pricingService: Pricing) extends Checkout {

  override def checkout(items: Seq[Item]): Long = {
    val priced = pricingService.priceItems(items)
    priced.foldLeft(0L){case (t, pi) => t + pi.price }
  }

  override def checkout(items: Seq[Item], batchOffers: Map[String, PricedItemBatch => Long]): Long = {
    pricingService.aggregatedPrice(items)
      .map(pib => batchOffers.get(pib.pricedItem.name).map(pf => pf(pib))
        .getOrElse(pib.pricedItem.price)).sum
  }

}
