package me.miquel.herrera.checkout

object TestPrices {
  val priceStore: Map[String, Long] = Map(
    "Apple" -> 60,
    "Orange" -> 25
  )
}
