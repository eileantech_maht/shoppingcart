package me.miquel.herrera.checkout

import me.miquel.herrera.checkout.Items.{Item, PricedItemBatch}
import me.miquel.herrera.checkout.pricing.{OfferTypes, PricingService}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CheckoutServiceTest extends AnyFlatSpec with Matchers {

  private val pricingService = PricingService(TestPrices.priceStore)
  private val checkoutService: Checkout = CheckoutService(pricingService)

  "A checkout service" should "give a total price for a list of items" in {
    val total = checkoutService.checkout(Seq(Item("Apple"), Item("Orange"), Item("Apple")))

    total shouldBe 145

  }

  it should "throw an exception if there are pricing issues" in {
    val error = intercept[RuntimeException] {
      checkoutService.checkout(Seq(Item("Orange"), Item("Apricot"), Item("Orange"), Item("Pear")))
    }

    error.getMessage shouldBe "Some items did not have a price associated: [Apricot, Pear]"
  }

  it should "provide correct price with offers" in {

    val offers: Map[String, PricedItemBatch => Long] = Map(
      "Apple" -> OfferTypes.buyOneGetOneFree,
      "Orange" -> OfferTypes.threeForTwo
    )

    val oneAppleOneOrange = Seq(Item("Apple"), Item("Orange"))
    val twoAppleOneOrange = Seq(Item("Apple"), Item("Orange"), Item("Apple"))
    val twoAppleTwoOrange = twoAppleOneOrange :+ Item("Orange")
    val twoAppleThreeOrange = twoAppleTwoOrange :+ Item("Orange")
    val threeAppleThreeOrange = twoAppleThreeOrange :+ Item("Apple")
    val fourAppleThreeOrange = threeAppleThreeOrange :+ Item("Apple")
    val fourAppleSixOrange = twoAppleThreeOrange ++ twoAppleThreeOrange

    val p1A1O = checkoutService.checkout(oneAppleOneOrange, offers)
    val p2A1O = checkoutService.checkout(twoAppleOneOrange, offers)
    val p2A2O = checkoutService.checkout(twoAppleTwoOrange, offers)
    val p2A3O = checkoutService.checkout(twoAppleThreeOrange, offers)
    val p3A3O = checkoutService.checkout(threeAppleThreeOrange, offers)
    val p4A6O = checkoutService.checkout(fourAppleSixOrange, offers)

    p1A1O shouldBe 85
    p2A1O shouldBe 85
    p2A2O shouldBe 110
    p2A3O shouldBe 110
    p3A3O shouldBe 170
    p4A6O shouldBe 220

  }

}
