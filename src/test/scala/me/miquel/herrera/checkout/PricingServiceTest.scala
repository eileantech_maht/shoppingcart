package me.miquel.herrera.checkout

import me.miquel.herrera.checkout.Items.{Item, PricedItem, PricedItemBatch}
import me.miquel.herrera.checkout.pricing.PricingService
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PricingServiceTest extends AnyFlatSpec with Matchers {

  private val pricingService = PricingService(TestPrices.priceStore)

  "A pricing service" should "map prices" in {

    val price = pricingService.priceItems(Seq(Item("Apple"), Item("Orange"), Item("Apple")))

    price shouldBe Seq(PricedItem("Apple", 60), PricedItem("Orange", 25), PricedItem("Apple", 60))

  }

  it should "tolerate empty item lists" in {
    val price = pricingService.priceItems(Seq())

    price shouldBe empty
  }

  it should "throw an exception with the list of items missing a price" in {
    val error = intercept[RuntimeException] {
      pricingService.priceItems(Seq(Item("Pineapple"), Item("Orange"), Item("Mango")))
    }

    error.getMessage shouldBe "Some items did not have a price associated: [Pineapple, Mango]"
  }

  it should "provide aggregated pricing" in {
    val aggPrices = pricingService.aggregatedPrice(Seq(Item("Apple"), Item("Orange"), Item("Apple"), Item("Orange"), Item("Apple")))

    aggPrices should have length(2)
    aggPrices should contain(PricedItemBatch(PricedItem("Apple",  60), 3))
    aggPrices should contain(PricedItemBatch(PricedItem("Orange", 25), 2))
  }

  it should "aggregate empty items as an empty aggregate" in {
    val aggPrices = pricingService.aggregatedPrice(Seq())

    aggPrices shouldBe empty
  }
}
